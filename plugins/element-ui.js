import Vue from 'vue'

import ElementUI from 'element-ui'
import locale from  '../assets/locale/en.js'

Vue.use(ElementUI, {locale})

module.exports = {

	head: {
		title: 'frontend',
		meta: [
			{charset: 'utf-8'},
			{name: 'viewport', content: 'width=device-width, initial-scale=1'},
			{hid: 'description', name: 'description', content: 'Test job application'}
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
			{ rel: 'stylesheet', href: 'https://unpkg.com/element-ui/lib/theme-chalk/index.css' }
		]
	},

	plugins: [
		'~plugins/element-ui.js'
	],

	modules: [
		'@nuxtjs/axios',
		'@nuxtjs/auth'
	],

	//axios: {
	//	host: '188.166.14.193'
	//},

	auth: {
		//redirect: {
		//	login: '/login',
		//	logout: '/login',
		//	callback: '/partners',
		//	home: '/partners'
		//},
		//resetOnError: true,
		strategies: {
			local: {
				endndpoints: {
					login: {
						url: '/auth',
						method: 'get',
						propertyName: 'token'
					}
				}
			}
		}
	},

	css: [
		'@/assets/css/base.styl',
		'@/assets/css/font-awesome.css'
	],

	loading: {color: '#3B8070'},

	build: {

		extend (config, {isDev, isClient}) {
			if (isDev && isClient) {
				config.module.rules.push({
					enforce: 'pre',
					test: /\.(js|vue)$/,
					loader: 'eslint-loader',
					exclude: /(node_modules)/
				});
			}
		}
	}
}


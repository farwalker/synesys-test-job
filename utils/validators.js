export function usernameValidator(rule, value, callback) {
	if (!value) {
		callback(new Error('Field is required'))
	} else if (/^[-_]/.test(value) || /[-_]$/.test(value)) {
		callback(new Error('Value cannot start or end with - or _'))
	} else if (!/^[0-9A-Za-z-_]+$/.test(value)) {
		callback(new Error('Accepts only letters, numbers, -, _'))
	} else {
		callback()
	}
}

export function emailValidator(rule, value, callback) {
	if (!value) {
		callback(new Error('Email is required'))
	} else if (!/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(value)) {
		callback(new Error('Invalid email address'))
	} else {
		callback()
	}
}

export function nameValidator(rule, value, callback) {
	if (!value) {
		callback(new Error('Field is required'))
	} else if (!/^[0-9A-Za-z]+$/.test(value)) {
		callback(new Error('Accepts only letters and numbers'))
	} else {
		callback()
	}
}

export function phoneValidator(rule, value, callback) {
	if (!value) {
		callback(new Error('Phone is required'))
	} else if (!/^[0-9]{10,10}$/.test(value)) {
		callback(new Error('Ivalid phone number format'))
	} else {
		callback()
	}
}